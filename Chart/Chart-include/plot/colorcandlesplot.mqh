/*
Copyright 2020 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

// Класс цветного свечного графика: DRAW_COLOR_CANDLES. © FXcoder
//TODO: выделить вариант для двух цветов в отдельный класс, наследующий этот

#property strict

#include "../bsl.mqh"
#include "plot.mqh"


class CColorCandlesPlot: public CPlot
{
public:

	double buffer_open[];
	double buffer_high[];
	double buffer_low[];
	double buffer_close[];


protected:

#ifdef __MQL4__
	double buffer_bull_body_u[];
	double buffer_bull_body_l[];
	double buffer_bear_body_u[];
	double buffer_bear_body_l[];
	double buffer_body_mask[];
	double buffer_bull_shadow_u[];
	double buffer_bull_shadow_l[];
	double buffer_bear_shadow_u[];
	double buffer_bear_shadow_l[];
	double buffer_shadow_mask[];
#else
	double buffer_color[];
#endif

	color bull_color_;
	color bear_color_;
	color back_color_;

public:

	// Конструктор по умолчанию (не инициализировать)
	void CColorCandlesPlot():
		CPlot(),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
	}

	void CColorCandlesPlot(int plot_index_first, int buffer_index_first):
		CPlot(),
		bull_color_(clrNONE),
		bear_color_(clrNONE),
		back_color_(clrNONE)
	{
		init(plot_index_first, buffer_index_first);
	}

	virtual CColorCandlesPlot *init(int plot_index_first, int buffer_index_first) override
	{
		init_properties();
		
#ifdef __MQL4__
		
		plot_index_first_ = plot_index_first;
		plot_index_count_ = 14; //todo: невидимые учитываются в 4?
		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 14;
			
		::SetIndexBuffer(buffer_index_first_ + 0, buffer_bull_body_u,   INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 1, buffer_bull_body_l,   INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 2, buffer_bear_body_u,   INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 3, buffer_bear_body_l,   INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 4, buffer_body_mask,     INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 5, buffer_bull_shadow_u, INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 6, buffer_bull_shadow_l, INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 7, buffer_bear_shadow_u, INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 8, buffer_bear_shadow_l, INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 9, buffer_shadow_mask,   INDICATOR_DATA);

		::SetIndexBuffer(buffer_index_first_ + 10, buffer_open,  INDICATOR_CALCULATIONS);
		::SetIndexBuffer(buffer_index_first_ + 11, buffer_high,  INDICATOR_CALCULATIONS);
		::SetIndexBuffer(buffer_index_first_ + 12, buffer_low,   INDICATOR_CALCULATIONS);
		::SetIndexBuffer(buffer_index_first_ + 13, buffer_close, INDICATOR_CALCULATIONS);
		
		for (int i = 0; i < 10; i++)
			SetIndexStyle(buffer_index_first_ + i, DRAW_HISTOGRAM);
			
		for (int i = 10; i < 14; i++)
			SetIndexStyle(buffer_index_first_ + i, DRAW_NONE);
			
		label("");
		
		// remove as series
		::ArraySetAsSeries(buffer_bull_body_u,   false);
		::ArraySetAsSeries(buffer_bull_body_l,   false);
		::ArraySetAsSeries(buffer_bear_body_u,   false);
		::ArraySetAsSeries(buffer_bear_body_l,   false);
		::ArraySetAsSeries(buffer_body_mask,     false);
		::ArraySetAsSeries(buffer_bull_shadow_u, false);
		::ArraySetAsSeries(buffer_bull_shadow_l, false);
		::ArraySetAsSeries(buffer_bear_shadow_u, false);
		::ArraySetAsSeries(buffer_bear_shadow_l, false);
		::ArraySetAsSeries(buffer_shadow_mask,   false);

		::ArraySetAsSeries(buffer_open,   false);
		::ArraySetAsSeries(buffer_high,   false);
		::ArraySetAsSeries(buffer_low,    false);
		::ArraySetAsSeries(buffer_close,  false);

#else
		
		plot_index_first_ = plot_index_first;
		plot_index_count_ = 1;
		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 5;
		
		::SetIndexBuffer(buffer_index_first_ + 0, buffer_open,  INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 1, buffer_high,  INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 2, buffer_low,   INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 3, buffer_close, INDICATOR_DATA);
		::SetIndexBuffer(buffer_index_first_ + 4, buffer_color, INDICATOR_COLOR_INDEX);
		
		draw_type(DRAW_COLOR_CANDLES); // после SetIndexBuffer //TODO: дополнить комментарий, почему это важно
		
#endif
		return(&this);
	}

	virtual CColorCandlesPlot *init(const CPlot &prev) override
	{
		return(init(prev.plot_index_next(), prev.buffer_index_next()));
	}

	// Размер буфера
	virtual int size() const override
	{
		// любой буфер подойдёт
		return(::ArraySize(buffer_open));
	}


	// bars - число последних баров для очистки, -1 = все
	virtual CColorCandlesPlot *empty(int bars = -1) override
	{
		double empty_value = empty_value();
		
		if (bars < 0)
		{
			::ArrayInitialize(buffer_open,  empty_value);
			::ArrayInitialize(buffer_high,  empty_value);
			::ArrayInitialize(buffer_low,   empty_value);
			::ArrayInitialize(buffer_close, empty_value);

#ifdef __MQL4__
			::ArrayInitialize(buffer_bull_body_u,   empty_value);
			::ArrayInitialize(buffer_bull_body_l,   empty_value);
			::ArrayInitialize(buffer_bear_body_u,   empty_value);
			::ArrayInitialize(buffer_bear_body_l,   empty_value);
			::ArrayInitialize(buffer_body_mask,     empty_value);
			::ArrayInitialize(buffer_bull_shadow_u, empty_value);
			::ArrayInitialize(buffer_bull_shadow_l, empty_value);
			::ArrayInitialize(buffer_bear_shadow_u, empty_value);
			::ArrayInitialize(buffer_bear_shadow_l, empty_value);
			::ArrayInitialize(buffer_shadow_mask,   empty_value);
#else
			::ArrayInitialize(buffer_color, 0);
#endif

		}
		else
		{
			// empty может быть вызван до инициализации буферов и первого тика
			int size = size();
			
			if (bars > size)
				bars = size;
			
			int offset = size - bars;

			::ArrayFill(buffer_open,  offset, bars, empty_value);
			::ArrayFill(buffer_high,  offset, bars, empty_value);
			::ArrayFill(buffer_low,   offset, bars, empty_value);
			::ArrayFill(buffer_close, offset, bars, empty_value);
			
#ifdef __MQL4__
			::ArrayFill(buffer_bull_body_u,   offset, bars, empty_value);
			::ArrayFill(buffer_bull_body_l,   offset, bars, empty_value);
			::ArrayFill(buffer_bear_body_u,   offset, bars, empty_value);
			::ArrayFill(buffer_bear_body_l,   offset, bars, empty_value);
			::ArrayFill(buffer_body_mask,     offset, bars, empty_value);
			::ArrayFill(buffer_bull_shadow_u, offset, bars, empty_value);
			::ArrayFill(buffer_bull_shadow_l, offset, bars, empty_value);
			::ArrayFill(buffer_bear_shadow_u, offset, bars, empty_value);
			::ArrayFill(buffer_bear_shadow_l, offset, bars, empty_value);
			::ArrayFill(buffer_shadow_mask,   offset, bars, empty_value);
#else
			::ArrayFill(buffer_color, offset, bars, 0);
#endif

		}

		return(&this);
	}


	virtual CColorCandlesPlot *empty_bar(int bar) override
	{
		double empty_value = empty_value();

		buffer_open  [bar] = empty_value;
		buffer_high  [bar] = empty_value;
		buffer_low   [bar] = empty_value;
		buffer_close [bar] = empty_value;

#ifdef __MQL4__
		buffer_bull_body_u   [bar] = empty_value;
		buffer_bull_body_l   [bar] = empty_value;
		buffer_bear_body_u   [bar] = empty_value;
		buffer_bear_body_l   [bar] = empty_value;
		buffer_body_mask     [bar] = empty_value;
		buffer_bull_shadow_u [bar] = empty_value;
		buffer_bull_shadow_l [bar] = empty_value;
		buffer_bear_shadow_u [bar] = empty_value;
		buffer_bear_shadow_l [bar] = empty_value;
		buffer_shadow_mask   [bar] = empty_value;
#else
		buffer_color[bar] = 0;
#endif

		return(&this);
	}


	// Переопределения

	// префикс + типовое имя буфера
	virtual CColorCandlesPlot *label(string prefix) override
	{
#ifdef __MQL4__
		
		string labels[14] =
		{
			"Bull Body",   "Bull Body 2",   "Bear Body",   "Bear Body 2",   "Body Mask",
			"Bull Shadow", "Bull Shadow 2", "Bear Shadow", "Bear Shadow 2", "Shadow Mask",
			"Open", "High", "Low", "Close"
		};
		
		// служебные буферы скрывать нельзя (по крайней мере, не все), иначе нельзя будет
		// кликать по индикатору для вызова параметров
		
		for (int i = 0; i < 14; i++)
			SetIndexLabel(buffer_index_first_ + i, prefix + labels[i]);
			
#else

		CPlot::label(prefix + "Open;" + prefix + "High;" + prefix + "Low;" + prefix + "Close");
		
#endif
		return(&this);
	}


	// true, если значения были изменены
	bool set_two_color(color bull_color, color bear_color)
	{
		color back_color = _chart.color_background();
		
		if ((bull_color == bull_color_) && (bear_color == bear_color_) && (back_color == back_color_))
			return(false);

		bull_color_ = bull_color;
		bear_color_ = bear_color;
		back_color_ = back_color;

#ifdef __MQL4__

//todo: кэш?
		int body_width = _chart.bar_body_line_width();
		
		SetIndexStyle(buffer_index_first_ + 0, DRAW_HISTOGRAM, EMPTY, body_width, bull_color);
		SetIndexStyle(buffer_index_first_ + 1, DRAW_HISTOGRAM, EMPTY, body_width, bull_color);
		SetIndexStyle(buffer_index_first_ + 2, DRAW_HISTOGRAM, EMPTY, body_width, bear_color);
		SetIndexStyle(buffer_index_first_ + 3, DRAW_HISTOGRAM, EMPTY, body_width, bear_color);
		SetIndexStyle(buffer_index_first_ + 4, DRAW_HISTOGRAM, EMPTY, body_width, back_color);
		SetIndexStyle(buffer_index_first_ + 5, DRAW_HISTOGRAM, EMPTY, EMPTY,      bull_color);
		SetIndexStyle(buffer_index_first_ + 6, DRAW_HISTOGRAM, EMPTY, EMPTY,      bull_color);
		SetIndexStyle(buffer_index_first_ + 7, DRAW_HISTOGRAM, EMPTY, EMPTY,      bear_color);
		SetIndexStyle(buffer_index_first_ + 8, DRAW_HISTOGRAM, EMPTY, EMPTY,      bear_color);
		SetIndexStyle(buffer_index_first_ + 9, DRAW_HISTOGRAM, EMPTY, EMPTY,      back_color);

#else

		color_indexes(2);
		line_color(0, bull_color);
		line_color(1, bear_color);

#endif

		return(true);
	}

	bool update_width()
	{
#ifdef __MQL4__

		static int width_prev = -1;
		int width = _chart.bar_body_line_width();
		
		if (width == width_prev)
			return(false);

		SetIndexStyle(buffer_index_first_ + 0, DRAW_HISTOGRAM, EMPTY, width);
		SetIndexStyle(buffer_index_first_ + 1, DRAW_HISTOGRAM, EMPTY, width);
		SetIndexStyle(buffer_index_first_ + 2, DRAW_HISTOGRAM, EMPTY, width);
		SetIndexStyle(buffer_index_first_ + 3, DRAW_HISTOGRAM, EMPTY, width);
		SetIndexStyle(buffer_index_first_ + 4, DRAW_HISTOGRAM, EMPTY, width);
		
		width_prev = width;

#endif

		return(true);
	}

	void set_candle(int bar, double open, double high, double low, double close)
	{
		buffer_open  [bar] = open;
		buffer_high  [bar] = high;
		buffer_low   [bar] = low;
		buffer_close [bar] = close;

#ifdef __MQL4__

		bool is_bull = close >= open;
		bool is_bear = !is_bull;

		double body_max = is_bear ? open : close;
		double body_min = is_bull ? open : close;
		
		double empty_value = empty_value();
		
		/*
		open	buffer_bull_body_l		is_bull && (open < 0)
				buffer_bear_body_u		is_bear && (open >= 0)
				
		close	buffer_bull_body_u		is_bull && (close >= 0)
				buffer_bear_body_l		is_bear && (close < 0)
		
		high	buffer_bull_shadow_u	is_bull && (high >= 0)
				buffer_bear_shadow_u	is_bear && (high >= 0)
				
		low		buffer_bull_shadow_l	is_bull && (low < 0)
				buffer_bear_shadow_l	is_bear && (low < 0)
		*/
		
		//TODO: необходимое смещение необходимо рассчитывать на основе значения смещения в 1 пиксель
		//TODO: возомжно, следует рисовать обычную линию в таких местах
//		if ((open == close) && (high == low))
//		{
//			double point = 10.0 * pow(10.0, -_indicator.digits());
//			high += point;
//			low -= point;
//		}
//
		
		buffer_bull_body_u   [bar] = (is_bull && (close >= 0)) ? close : empty_value; // BullBodyU   // часть бычьей свечи над 0
		buffer_bull_body_l   [bar] = (is_bull && (open < 0))   ? open  : empty_value; // BullBodyL   // часть бычьей свечи под 0
		
		buffer_bear_body_u   [bar] = (is_bear && (open >= 0)) ? open  : empty_value; // BearBodyU    // часть межвежей свечи над 0
		buffer_bear_body_l   [bar] = (is_bear && (close < 0)) ? close : empty_value; // BearBodyL    // часть межвежей свечи под 0
		
		buffer_body_mask     [bar] = (body_min > 0) ? body_min : (body_max < 0 ? body_max : empty_value); // BodyMask
		
		
		buffer_bull_shadow_u [bar] = (is_bull && (high >= 0)) ? high : empty_value; // BullShadow    // верхний хвост бычьей свечи выше 0
		buffer_bull_shadow_l [bar] = (is_bull && (low < 0))   ? low  : empty_value; // BullShadow2   // нижний хвост бычьей свечи ниже 0
		
		buffer_bear_shadow_u [bar] = (is_bear && (high >= 0)) ? high : empty_value; // BearShadow    // верхний хвост медв. свечи выше 0
		buffer_bear_shadow_l [bar] = (is_bear && (low < 0))   ? low  : empty_value; // BearShadow2   // нижний хвост медв свечи ниже 0
		
		buffer_shadow_mask   [bar] = low >= 0 ? low : (high < 0 ? high : empty_value); // ShadowMask

#else

		buffer_color [bar] = close < open ? 1 : 0;

#endif
	}

	void set_candle(int bar, const MqlRates &rate)
	{
		set_candle(bar, rate.open, rate.high, rate.low, rate.close);
	}

	// Добавить значение к указанному бару
	void set_value(int bar, double value, bool reset = false)
	{
		double empty_value = empty_value();
		
		if (reset || (value == empty_value) || !is_valid_bar(bar))
		{
			set_candle(bar, value, value, value, value);
			return;
		}

		buffer_close[bar] = value;
		
		if (value > buffer_high[bar])
			buffer_high[bar] = value;
		else if (value < buffer_low[bar])
			buffer_low[bar] = value;


#ifdef __MQL4__

		// определить, где находятся значения ohlc, считать их и установить новый бар заново (или частично)
		
		
		// some magic...
		
		/*
		Имитация рисования свечи барами с перекрытием и затенением ниже хвостов
		open-close scheme (see candles_mt4.xlsx)"
		BullBodyU                              close  close
		BullBodyL                       open   open          1
		BearBodyU         open   open                        2
		BearBodyL  close  close
		BodyMask   open          close  close         open   3
		*/
		
		// Цена закрытия будет переопределена, поэтому нет смысла её искать
		double open = buffer_bull_body_l[bar];
		
		if (open == empty_value)
		{
			open = buffer_bear_body_u[bar];
			
			if (open == empty_value)
			{
				open = buffer_body_mask[bar];
			}
		}
		
		
		/*
		high-low scheme (see candles_mt4.xlsx)"
		BullShadowU                         high  high  1
		BullShadowL                    low  low
		BearShadowU        high  high                   2
		BearShadowL  low   low
		ShadowMask   high        low   high       low   3
		*/
		
		
		double low = empty_value;
		double high = buffer_bull_shadow_u[bar];
		
		if (high == empty_value)
		{
			high = buffer_bear_shadow_u[bar];
			
			if (high == empty_value)
			{
				high = buffer_shadow_mask[bar];
				low = buffer_bull_shadow_l[bar];
				
				if (low == empty_value)
					low = buffer_bear_shadow_l[bar];
			}
			else
			{
				low = buffer_bear_shadow_l[bar];
				
				if (low == empty_value)
					low = buffer_shadow_mask[bar];
			}
		}
		else
		{
			low = buffer_bull_shadow_l[bar];
			
			if (low == empty_value)
				low = buffer_shadow_mask[bar];
		}
		
		double close = value;

		if (value > high)
			high = value;
		else if (value < low)
			low = value;
		
		set_candle(bar, open, high, low, close);

#else

		buffer_color[bar] = value >= buffer_open[bar] ? 0 : 1; //TODO: это только для двухцветного режима, добавить варианты

#endif
	}

	void set_last(double last, bool reset)
	{
		set_value(0, last, reset);
	}

	bool is_valid_bar(int bar)
	{
		double empty_value = empty_value(); //TODO: или просто EMPTY_VALUE?

		return
		(
			(buffer_open [bar] != empty_value) &&
			(buffer_high [bar] != empty_value) &&
			(buffer_low  [bar] != empty_value) &&
			(buffer_close[bar] != empty_value)
		);
	}

#ifdef __MQL4__
	
	//TODO: менять draw_begin?
	virtual CColorCandlesPlot *shift(int value) override
	{
		shift_ = value;

		for (int i = 0; i < buffer_index_count_; i++)
			SetIndexShift(buffer_index_first_ + i, value);
		
		return(&this);
	}

	//TODO: в мт4 +shift?
	virtual CColorCandlesPlot *draw_begin(int value) override
	{
		draw_begin_ = value;

		for (int i = 0; i < buffer_index_count_; i++)
		{
			SetIndexDrawBegin(buffer_index_first_ + i, value + shift_);
		}
		
		return(&this);
	}

#else

#endif

};
